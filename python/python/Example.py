import logging
import paho.mqtt.client as mqtt
import time

from gen.IHelloWorld import IHelloWorldPublisher
from gen.IHelloWorld import IHelloWorldReceiver
from gen.IHelloWorld import IHelloWorldCallback


class HelloWorldCallback (IHelloWorldCallback):
    
    def onGreet(self, greetingText):
        logging.info("I am consuming onGreet {}".format(greetingText))
        
    def onAnotherGreet(self, greetingText, greetingNumber):
        logging.info("I am consuming onAnotherGreet {} {}".format(greetingText, greetingNumber))
        
    def onAndOneMoreGreet(self, text1, text2, number):
        logging.info("I am consuming onAndOneMoreGreet {} {} {}".format(text1, text2, number))
    
if __name__ == '__main__':
    
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', filename='traces.log',filemode='w', level=logging.DEBUG)
        
        client = mqtt.Client("HelloWorldApplication")        
        client.connect('iot.eclipse.org')
        
        client.loop_start()

        consumer = IHelloWorldReceiver()
        consumer.registerForCallbacks(client, HelloWorldCallback() )
        
        provider = IHelloWorldPublisher(client)
        
        provider.fireGreet("Hello World")
        provider.fireAnotherGreet("Hello World", 42)
        provider.fireAndOneMoreGreet("Hello", "World", 42)
        
        time.sleep(15)
        
        client.loop_stop()
