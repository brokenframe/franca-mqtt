package org.tomo.franca2mqtt.common

import org.franca.core.franca.FArgument
import org.franca.core.franca.FBroadcast
import org.franca.core.franca.FInterface

interface INaming {

	def String topicPath(FBroadcast b)
	
	def String publishMethod(FBroadcast b)

	def String publishClass(FInterface fi)

	def String receiverClass(FInterface fi)

	def String callbackMethod(FBroadcast b)

	def String callbackClass(FInterface fi)
	
	def String getEventName(FBroadcast b)

	def String getTypeScriptProxy(FInterface fi)
	
	def String getJavaScriptEventHandler(FInterface fi)	
	
	def String getHTML(FInterface fi)
	
	def String getDiv(FInterface fi)

	def String getButton(FBroadcast b)
	
	def String getFieldId(FBroadcast b, FArgument a)
	
}
