package org.tomo.franca2mqtt.common

import org.franca.core.franca.FBroadcast
import org.franca.core.franca.FInterface
import org.franca.core.franca.FModel
import org.franca.core.franca.FArgument

class DefaultNaming implements INaming {
		
	override topicPath(FBroadcast b) {
		val fi = b.eContainer as FInterface
		val _package = (fi.eContainer as FModel).name		
		"'"+_package.replace('.','/')+'/'+fi.name.toLowerCase+'/'+b.name+"'"
	}
	
	override publishMethod(FBroadcast b) {
		'fire'+b.name.toFirstUpper
	}
	
	override publishClass(FInterface fi) {
		'I'+fi.name+'Publisher'
	}
	
	override receiverClass(FInterface fi) {
		'I'+fi.name+'Receiver'
	}
	
	override callbackClass(FInterface fi) {
		'I'+fi.name+'Callback'
	}
	
	override callbackMethod(FBroadcast b) {
		'on'+b.name.toFirstUpper
	}
	
	override getEventName(FBroadcast b) {
		b.name.toUpperCase+'_EVENT'
	}
	
	override getTypeScriptProxy(FInterface fi) {
		fi.name+'Proxy'
	}
	
	override getJavaScriptEventHandler(FInterface fi) {
		fi.name.toLowerCase+'_handler'
	}
	
	override getHTML(FInterface fi) {
		fi.name+'UI'
	}
	
	override getDiv(FInterface fi) {
		fi.name+'_div'
	}
	
	override getButton(FBroadcast b) {
		b.publishMethod+'Btn'
	}
	
	override getFieldId(FBroadcast b, FArgument a) {
		b.publishMethod + '_' + a.name
	}
		
	
}
