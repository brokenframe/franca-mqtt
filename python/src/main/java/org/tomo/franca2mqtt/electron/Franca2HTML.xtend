package org.tomo.franca2mqtt.electron

import java.io.FileWriter
import org.franca.core.franca.FBroadcast
import org.franca.core.franca.FEnumerationType
import org.franca.core.franca.FInterface
import org.franca.core.franca.FModel
import org.tomo.franca2mqtt.common.INaming

class Franca2HTML {
	
	extension val INaming naming

	new(INaming _naming) {
		naming = _naming
	}

	def toFile(FModel model, String path) {
				
		val code = model.interfaces.get(0).generate		
		val fw = new FileWriter(path+'/'+model.interfaces.get(0).HTML+'.html')		
		fw.write(code.toString)
		fw.close
	}

	def generateTriggerForm(FBroadcast b) {
		'''
			<div>
				<form>
					<input type="button" id="�b.button�" value="�b.publishMethod�" />
					�FOR a : b.outArgs�
						�IF a.type.derived != null�
							<select id="�b.getFieldId(a)�">
								�FOR e: (a.type.derived as FEnumerationType).enumerators�
									<option>�e.name�</option>
								�ENDFOR�
							</select>
						�ELSE�
							<input type="text" id="�b.getFieldId(a)�" value="�a.name�"/>
						�ENDIF�
					�ENDFOR�
				</form>
			</div>
		'''
	}


	def generate(FInterface fi) {
		'''
		�FOR b : fi.broadcasts�
			�b.generateTriggerForm�
		�ENDFOR�
		'''
	}	
}