package org.tomo.franca2mqtt.electron

import java.io.FileWriter
import org.franca.core.franca.FBroadcast
import org.franca.core.franca.FInterface
import org.franca.core.franca.FModel
import org.tomo.franca2mqtt.common.INaming

class Franca2JavaScriptHandler {

	extension val INaming naming

	new(INaming _naming) {
		naming = _naming
	}

	def toFile(FModel model, String path) {
				
		val code = model.interfaces.get(0).generate		
		val fw = new FileWriter(path+'/'+model.interfaces.get(0).javaScriptEventHandler+'.js')		
		fw.write(code.toString)
		fw.close
	}

	def generate(FInterface fi) {
		'''
		const genProxy = require('./�fi.typeScriptProxy�')
		
		var $ = require('jquery')
		
		function initialize(client) {
		    var proxy = new genProxy.�fi.typeScriptProxy�(client)      
		
		    $("#�fi.div�").load("gen/�fi.HTML�.html");
		    
		    �FOR b: fi.broadcasts�
		    �b.generateFireMethod�
		    �ENDFOR�
		    
		}
		module.exports.initialize = initialize
		'''
	}
	
	def generateFireMethod(FBroadcast b) {
		'''
    	$('#�b.button�').on('click', () => {
    		�FOR a:b.outArgs�
    		var �a.name� = $('#�b.getFieldId(a)�').val()
    		�ENDFOR�
    		proxy.�b.publishMethod�(�b.outArgs.map[name].join(', ')�)
    	})
		'''
	}	
		
}