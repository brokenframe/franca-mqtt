package org.tomo.franca2mqtt.electron

import java.io.FileWriter
import org.franca.core.franca.FArgument
import org.franca.core.franca.FBasicTypeId
import org.franca.core.franca.FBroadcast
import org.franca.core.franca.FModel
import org.tomo.franca2mqtt.common.INaming
import org.franca.core.franca.FInterface

class Franca2TypeScriptProxy {

	extension val INaming naming

	new(INaming _naming) {
		naming = _naming
	}

	def toFile(FModel model, String path) {
				
		val code = model.interfaces.get(0).generate		
		val fw = new FileWriter(path+'/'+model.interfaces.get(0).typeScriptProxy+'.ts')		
		fw.write(code.toString)
		fw.close
	}

	def generate(FInterface fi) {
		'''
		import { MqttClient } from "mqtt";

		class �fi.typeScriptProxy� {

			client: MqttClient
			
			�FOR b:fi.broadcasts�
			�b.eventName�:string = �b.topicPath�
			�ENDFOR�
		
		    constructor(client: MqttClient) {
		        this.client = client
		    }
		    
			�FOR b: fi.broadcasts�
			�b.generateFireMethod�
			
			�ENDFOR�
		
		}
		
		export { �fi.typeScriptProxy� };
		'''
	}

	def generateFireMethod(FBroadcast b) {
		'''
		    �b.publishMethod�(�b.outArgs.map[argument].join(', ')�) {
		        var payload = {
		        	�FOR a: b.outArgs SEPARATOR ','�
		        	�a.name�: _�a.name�
		        	�ENDFOR�
		        };
		        this.client.publish(this.�b.eventName�, JSON.stringify(payload))
		    }
		'''
	}

	def argument(FArgument a) {
		'_'+a.name+': '+a.type.predefined.basicType
	}
	
	def basicType(FBasicTypeId type) {
		switch (type) {
			case BOOLEAN: 'boolean'
			case STRING: 'string'
			case DOUBLE,
			case FLOAT,
			case INT16,
			case INT32,
			case INT64, 
			case INT8,			
			case UINT16, 
			case UINT32, 
			case UINT64, 
			case UINT8: 'number' 			
			default: {
				'undefined'
			}
		}
	}

}
