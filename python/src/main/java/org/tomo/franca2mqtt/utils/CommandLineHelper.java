package org.tomo.franca2mqtt.utils;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CommandLineHelper {

	private String[] args = null;
	private Options options = new Options();
	
	private String francaFolder = null;
	private String pythonFolder = null;
	private String electronFolder = null;

	private final static String HELP_OPTION = "h";
	private final static String FRANCA_FOLDER_OPTION = "franca";
	private final static String PYTHON_FOLDER_OPTION = "python";
	private final static String ELECTRON_FOLDER_OPTION = "electron";
	
	public CommandLineHelper(String[] args) {

		this.args = args;

		options.addOption(HELP_OPTION, false, "show help.");
		options.addOption(FRANCA_FOLDER_OPTION, true, "Specify the folder with the franca files");
		options.addOption(PYTHON_FOLDER_OPTION, true, "Specify the folder for the generated python code");
		options.addOption(ELECTRON_FOLDER_OPTION, true, "Specify the folder for the generated electron code");

	}

	public String getElectronFolder() {
		return electronFolder;
	}

	public String getFrancaFolder() {
		return francaFolder;
	}
	
	public String getPythonFolder() {
		return pythonFolder;
	}
	
	public void parse() {
		CommandLineParser parser = new DefaultParser();

		CommandLine cmd = null;
		try {
			cmd = parser.parse(options, args);

			if (cmd.hasOption(HELP_OPTION))
				help();

			if (cmd.hasOption(FRANCA_FOLDER_OPTION)) {
				francaFolder = cmd.getOptionValue(FRANCA_FOLDER_OPTION);
			} 
			
			if (cmd.hasOption(PYTHON_FOLDER_OPTION)) {
				pythonFolder = cmd.getOptionValue(PYTHON_FOLDER_OPTION);
			}
			
			if (cmd.hasOption(ELECTRON_FOLDER_OPTION)) {
				electronFolder = cmd.getOptionValue(ELECTRON_FOLDER_OPTION);
			} 
			
			else {
				help();
			}

		} catch (ParseException e) {
			help();
		}
	}

	private void help() {
		// This prints out some help
		HelpFormatter formater = new HelpFormatter();

		formater.printHelp("Main", options);
		System.exit(0);
	}
}
