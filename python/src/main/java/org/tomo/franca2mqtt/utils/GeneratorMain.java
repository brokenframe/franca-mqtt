package org.tomo.franca2mqtt.utils;

import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.xtext.junit4.InjectWith;
import org.franca.core.dsl.FrancaIDLHelpers;
import org.franca.core.dsl.FrancaIDLInjectorProvider;
import org.franca.core.franca.FModel;
import org.tomo.franca2mqtt.common.DefaultNaming;
import org.tomo.franca2mqtt.electron.Franca2HTML;
import org.tomo.franca2mqtt.electron.Franca2JavaScriptHandler;
import org.tomo.franca2mqtt.electron.Franca2TypeScriptProxy;
import org.tomo.franca2mqtt.python.Franca2PyhonMqtt;


@InjectWith(FrancaIDLInjectorProvider.class)
public class GeneratorMain {


	public static void main(String[] args) {
		// TODO Auto-generated method stub

		CommandLineHelper commandLineParser = new CommandLineHelper(args);
		
		commandLineParser.parse();
		
		String francaFolder = commandLineParser.getFrancaFolder();
		String pythonFolder = commandLineParser.getPythonFolder();
		String electronFolder = commandLineParser.getElectronFolder();
		
		FileScanner scanner = new FileScanner();
		
		List<String> francaFiles = null;
		
		if(francaFolder != null) {
			francaFiles  = scanner.getFiles(francaFolder);			
		}
		else {
			System.exit(0);
		}
		
		if(pythonFolder != null) {
			Franca2PyhonMqtt pythonGenerator = new Franca2PyhonMqtt(new DefaultNaming());
			for(String f: francaFiles) {
				pythonGenerator.toFile(GeneratorMain.loadFrancaModel(f), pythonFolder);
			}			
		}
		
		if(electronFolder != null) {
			Franca2HTML htmlGenerator = new Franca2HTML(new DefaultNaming());
			Franca2JavaScriptHandler javascriptGenerator = new Franca2JavaScriptHandler(new DefaultNaming());
			Franca2TypeScriptProxy typeScriptGenerator = new Franca2TypeScriptProxy(new DefaultNaming());
			for(String f: francaFiles) {
				htmlGenerator.toFile(GeneratorMain.loadFrancaModel(f), electronFolder);
				javascriptGenerator.toFile(GeneratorMain.loadFrancaModel(f), electronFolder);
				typeScriptGenerator.toFile(GeneratorMain.loadFrancaModel(f), electronFolder);
			}			
		}
		
						
	}

	public static FModel loadFrancaModel(String filename) {
				
		FrancaIDLHelpers loader = FrancaIDLHelpers.instance();
		
		URI root = URI.createURI("file:/");
		URI loc = URI.createFileURI(filename);
		FModel fmodel = loader.loadModel(loc, root);
		return fmodel;
	}

	
}
