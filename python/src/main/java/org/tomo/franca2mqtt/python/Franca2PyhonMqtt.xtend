package org.tomo.franca2mqtt.python

import java.io.FileWriter
import org.franca.core.franca.FBroadcast
import org.franca.core.franca.FInterface
import org.franca.core.franca.FModel
import org.tomo.franca2mqtt.common.INaming
import org.franca.core.franca.FModelElement
import org.franca.core.franca.FAnnotationType
import org.franca.core.franca.FArgument
import org.franca.core.franca.FAnnotation

class Franca2PyhonMqtt {

	extension val INaming naming

	new (INaming _naming) {
		naming = _naming
	}

	def toFile(FModel model, String path) {
				
		val code = model.generate		
		val fw = new FileWriter(path+'/I'+model.interfaces.get(0).name+'.py')		
		fw.write(code.toString)
		fw.close
	}

	def generate(FModel fmodel) {
		'''
		�IF fmodel.interfaces.get(0).hasComments�
		"""
		�fmodel.interfaces.get(0).description�
		"""
		�ENDIF�
		import json
		import logging
		
		�fmodel.interfaces.get(0).generateTopics(fmodel.name)�
		'''
	}

	def generateTopics(FInterface fi, String _package) {
		'''
		�FOR b: fi.broadcasts�
		�b.eventName� = �b.topicPath�
		�ENDFOR�
		ALL_EVENTS = '�_package.replace('.','/')�/�fi.name.toLowerCase�/#'
		
		�fi.generatePublisher�

		�fi.generateReceiver�
		
		�fi.generateCallbackInterface�
		'''
	}
	
	def generatePublisher(FInterface fi) {				
		'''
		class �fi.publishClass� ():		
		    """
		    This interface must be implemented from a service, that wants to send corresponding broadcast messages 
		    """
		    
		    def __init__(self, client):
		        self.__client = client
		    
		    �FOR b: fi.broadcasts�
		    �b.generateFireMethod�
		    �ENDFOR�
		'''
	}
	
	def generateFireMethod(FBroadcast b) {
		'''
		def �b.publishMethod�(self, �b.outArgs.map[name].join(', ')�):
			�b.generateInlineDoc�
			payload = {}
			�FOR a: b.outArgs�
			payload['�a.name�'] = �a.name�
			�ENDFOR�
			logging.debug('%s.�b.publishMethod�', __name__)
			logging.debug('%s %s', �b.eventName�, json.dumps(payload))
			self.__client.publish(�b.eventName�, json.dumps(payload))
			
		'''
	}
	
	def generateReceiver(FInterface fi) {
		'''
		class �fi.receiverClass� ():
		
		    def registerForCallbacks(self, client, callback):
		        client.subscribe(ALL_EVENTS)
		        client.message_callback_add(ALL_EVENTS, self.__dispatch)
		        self.__callback = callback
		    
		    �fi.generateDispatcher�
		    		
		
		'''
	}
	
	def generateDispatcher(FInterface fi) {
		'''
		def __dispatch(self, client, userdata, message):
			value = str(message.payload.decode('utf-8'))
			t = message.topic
			�FOR b: fi.broadcasts�
			if t == �b.eventName�:
				data = json.loads(value)
				logging.debug('%s.�b.callbackMethod�', __name__)
				self.__callback.�b.callbackMethod�(�b.outArgs.map["data['"+name+"']"].join(', ')�)
			�ENDFOR�
		'''
	}
	
	def generateCallbackInterface(FInterface fi) {
		'''
		class �fi.callbackClass� ():
			"""
			This interface must be implemented from clients which want to consume the corresponding broadcast messages
			"""
			
			�FOR b:fi.broadcasts�
			�b.generateOnFiredEvents�
			�ENDFOR�
		
		'''
	}
	
	def generateOnFiredEvents(FBroadcast b) {
		'''
		def �b.callbackMethod�(self, �b.outArgs.map[name].join(', ')�):
			�b.generateInlineDoc�
			raise NotImplementedError("Please Implement this method")
			
		'''
	}
	
	
	private def generateInlineDoc(FBroadcast b) {
		'''
		�IF b.hasComments�
		"""
		�b.description�
			
		�FOR d: b.paramDescription�
		:param �d.comment�
		�ENDFOR�
		"""
		�ENDIF�		
		'''
	}
	
	private def hasComments(FModelElement ele) {
		ele.comment !== null && ele.comment.elements.size > 0
	}
	
	private def getDescription(FModelElement ele) {
		ele.comment.elements.filter[type.equals(FAnnotationType.DESCRIPTION)].head.comment
	}
	
	private def getParamDescription(FBroadcast b)
	{
		b.comment.elements.filter[type.equals(FAnnotationType.PARAM)]
	}
	
}
