package org.tomo.franca2mqtt.electron;

import static org.junit.Assert.*;

import java.nio.file.Paths;

import org.eclipse.emf.common.util.URI;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.franca.core.dsl.FrancaIDLInjectorProvider;
import org.franca.core.dsl.FrancaPersistenceManager;
import org.franca.core.franca.FModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tomo.franca2mqtt.common.DefaultNaming;

import com.google.inject.Inject;

@RunWith(XtextRunner.class)
@InjectWith(FrancaIDLInjectorProvider.class)
public class Franca2JavaScriptHandlerTest {

	@Inject
	FrancaPersistenceManager loader;

	final String FRANCA_MODEL = Paths.get("").toAbsolutePath().toString() + "/" +"src/test/resources/HelloWorld.fidl";
	final String GEN_PATH = "electron/gen";
	
	
	@Test
	public void testTransform() {
		transform(FRANCA_MODEL);
				
	}
	
	public void transform(String file) {
    	URI root = URI.createURI("file:/");
    	URI loc = URI.createFileURI(file);
    	FModel fmodel = loader.loadModel(loc, root);
		assertNotNull(fmodel);
		
		Franca2JavaScriptHandler generator = new Franca2JavaScriptHandler(new DefaultNaming());				
		generator.toFile(fmodel, GEN_PATH);
				
	}

}
