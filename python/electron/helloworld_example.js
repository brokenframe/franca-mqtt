var $ = require('jquery')
var mqtt = require('mqtt')

var example = require('./gen/helloworld_handler')


$("#HelloWorld_div").load("gen/HelloWorldUI.html");

$('#connectButton').on('click', () => {

  $('#Debug_div').text("Trying to connect...")

  let client  = mqtt.connect('mqtt://iot.eclipse.org')

  client.on('connect', function () {
    $('#Debug_div').text('Connected to the broker')    
    example.initialize(client)
  })  

  
})
