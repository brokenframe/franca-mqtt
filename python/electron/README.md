# Create and start the example application

## Install pre-requisites

* Node.js
* npm
* electron
* TypeScript

## Generate artifacts from Franca interface

Generate the javascript, typescript and html files from the given Franca interface.
Goto root directory and invoke 

```gradle build```

This should generate all artifacts into the folder ```./electron/gen```

## Start the application

```cd electron```

```npm install``` installs required packages

```tsc``` transpiles typescript *.ts files into javascript files *.js

Then execute with ```electron main.js```
