
# Purpose

This repository contains a collection of code generators, which can be used to transform software interfaces defined with [Franca IDL](https://github.com/franca/franca) into MQTT messaging.
The idea is to hide the MQTT implementation details (like topic name creation and subscription mechanisms) from the given interface description.

The first generator will create python code, for both publisher and subscriber.
The generated code is used for generating topics, encoding the payload as json and publishing the messages to a broker.
On the other hand it also waits for incoming messages, decodes the payload to data types and redirects the message to a callback method.

# Example

In order to generate the python code from the [example franca interface](https://gitlab.com/brokenframe/franca-mqtt/blob/master/python/src/test/resources/HelloWorld.fidl) you need to

* Clone this repository ```git clone https://gitlab.com/brokenframe/franca-mqtt.git```
* Go to the directory python:```cd python``` 
* Build the project with gradle: ```gradle build```

The python code for the example will then be generated into the folder ```python/python/gen``` 

In ```python/python/Example.py``` you will find following example:

```python
import logging
import paho.mqtt.client as mqtt
import time

from gen.IHelloWorld import IHelloWorldPublisher
from gen.IHelloWorld import IHelloWorldReceiver
from gen.IHelloWorld import IHelloWorldCallback

class HelloWorldCallback (IHelloWorldCallback):
    
    def onGreet(self, greetingText):
        logging.info("I am consuming onGreet")
        
    def onAnotherGreet(self, greetingText, greetingNumber):
        logging.info("I am consuming onAnotherGreet")
        
    def onAndOneMoreGreet(self, text1, text2, number):
        logging.info("I am consuming onAndOneMoreGreet")
    
if __name__ == '__main__':
    
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', filename='traces.log',filemode='w', level=logging.DEBUG)
        
        client = mqtt.Client("HelloWorldApplication")        
        client.connect('iot.eclipse.org')
        
        client.loop_start()

        consumer = IHelloWorldReceiver()
        consumer.registerForCallbacks(client, HelloWorldCallback() )
        
        provider = IHelloWorldPublisher(client)
        
        provider.fireGreet("Hello World")
        provider.fireAnotherGreet("Hello World", 42)
        provider.fireAndOneMoreGreet("Hello", "World", 42)
        
        time.sleep(5)
        
        client.loop_stop()
```

You also need to install the python mqtt library as additional dependency if you want to try the example and see what is happening.

```sudo pip install paho-mqtt```

# Conventions

## Topics

The topic name of a message will be generated like that:

```package name + interface name + broadcast name```

Means the topic name of following franca broadcast

```java
package org.tomo.mqtt.example

interface HelloWorld {	
    broadcast anotherGreet {
		out {
			String greetingText
			Int32 greetingNumber
		}
    }
}
```

will be transformed into  ```'org/tomo/mqtt/example/HelloWorld/anotherGreet'```

## Payload

The (out) parameters of the broadcast will be encoded into a JSON String before publishing the message

```json
{"greetingNumber": 42, "greetingText": "Hello World"}
```

## Publisher

For each franca interface a corresponding python class will be generated, that contains the methods for publishing the topics

```python
class IHelloWorldPublisher ():
#...
    def fireAnotherGreet(self, greetingText, greetingNumber):
    	payload = {}
    	payload['greetingText'] = greetingText
    	payload['greetingNumber'] = greetingNumber
    	logging.debug('%s.fireAnotherGreet', __name__)
    	logging.debug('%s %s', ANOTHERGREET_EVENT, json.dumps(payload))
    	self.__client.publish(ANOTHERGREET_EVENT, json.dumps(payload))
#...
```

Means, if you want to publish a message, you can just invoke the corresponding python method

```python
    provider = IHelloWorldPublisher(client)        
    provider.fireAnotherGreet("Hello World", 42)
```

## Consumer

For each franca interface also a corresponding python class will be generated, that contains the callback for processing the topics you have subscribed to.

```python
class IHelloWorldCallback ():
#...		
	def onAnotherGreet(self, greetingText, greetingNumber):
		raise NotImplementedError("Please Implement this method")
#...
```

If you want to consume messages you need to implement the methods of the generated callback class

```python
class HelloWorldCallback (IHelloWorldCallback):

#...    
    def onAnotherGreet(self, greetingText, greetingNumber):
        logging.info("I am consuming onAnotherGreet")
#...        
```

And finally you need to register your callback class, that it can be invoked automatically

```python
    consumer = IHelloWorldReceiver()
    consumer.registerForCallbacks(client, HelloWorldCallback() )
```

